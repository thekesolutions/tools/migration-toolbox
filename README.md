# migration-toolbox

This project provides a Dockerfile that adds all the things to a base Docker
image, suitable for playing with data.

## Usage

If you have your migration project in /home/user/migration, then you need to run:

```
    $ cd /home/user/migration
    $ docker run -it \
           -v $(pwd):/app \
           registry.gitlab.com/thekesolutions/tools/migration-toolbox \
           bash
```

You will find your project in _/app_

## Credits

This image is just a convenient way to run [ByWater's](https://bywatersolutions.com) 
[migration tools](https://github.com/bywatersolutions/Migration_Tools). Kudos to that
great company.
