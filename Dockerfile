# Base it on Debian 10
FROM perl:5.32

# File Author / Maintainer
LABEL maintainer="tomascohen@theke.io"

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin
ENV DEBIAN_FRONTEND noninteractive

# Set suitable debian sources
RUN echo "deb http://httpredir.debian.org/debian bullseye main" > /etc/apt/sources.list

ENV REFRESHED_AT 2023-03-20

RUN apt-get -y update \
    && apt-get -y install \
        yaz \
        cpanminus \
        less \
        locales \
        hexedit \
        vim \
        git \
        libtext-csv-xs-perl \
        perl-doc \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen \
    && dpkg-reconfigure locales \
    && /usr/sbin/update-locale LANG=en_US.UTF-8

RUN mkdir /tools
RUN    cd /tools \
    && git clone https://github.com/bywatersolutions/Migration_Tools.git

RUN cpanm -i --force \
        Modern::Perl \
        MARC::Record \
        MARC::File::XML \
        Text::Trim \
        Text::Iconv \
        Data::Printer

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

CMD ["/bin/bash"]
